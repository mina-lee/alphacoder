*alphacoder.txt* A short multi line description of your plugin

===============================================================================
CONTENTS                                                           *alphacoder*

    1. Intro ............................................... |alphacoder-intro|
    2. Requirements ................................. |alphacoder-requirements|
    3. Usage ............................................... |alphacoder-usage|
    4. Licence ........................................... |alphacoder-licence|
===============================================================================
1. Intro                                                     *alphacoder-intro*

Overview of plugin

2. Requirements                                       *alphacoder-requirements*

What additional programs are required to run the plugin

3. Usage                                                     *alphacoder-usage*

How to use the plugin. What functions does it give the user?

4. Licence                                                 *alphacoder-licence*

This is open source make sure to include a licence

vim:ft=help
