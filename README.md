
# AlphaCoder

For beta users (those who have the access to the server): please ssh to **jagupard7** and try it out there!

## Quick Start

1. Install [Vundle](https://github.com/gmarik/vundle) (vim plugin manager)

2. Add AlphaCoder plugin

    * Add `Plugin 'https://mina-lee@bitbucket.org/mina-lee/alphacoder.git'` to .vimrc
    * Run `:PluginInstall`

In order to update AlphaCoder plugin after installation, simply

  * Run `:PluginInstall!`

## Usage

In _normal_ or _insert_ mode of vim, type any keywords and

* Press `ctrl-tt` (hold `ctrl` key and press `t` twice) to autocomplete.
* __[Debug Mode]__ Press `ctrl-dd` to insert top five suggestions as comments.
  
```
  for i 10  
  if path exists
  if name main 
```

which generates:
  
```
  for i in range(10):  
  if os.path.exists(path):  
  if __name__ == "__main__":
```

## _oopsie! `raise NotImplementedError`_

Things that I plan to add someday. :)
  
1. Customization 
    * Allow users to easily change key mapping

2. Usability
    * Popup dropdown menu for suggestions
    * Select \<unknown\> part as a block and put a cursor there

3. Functionality
    * Build a local server
    * Show available models and let users choose
