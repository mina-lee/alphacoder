" --------------------------------
" Check basic requirements
" --------------------------------
if !has('python')
    echo 'vim has to be compiled with +python for this plugin'
    finish
endif

" ensure plugin is initialized only once
if exists('g:alphacoder_plugin_loaded')
    finish
endif

" --------------------------------
" Add our plugin to the path
" --------------------------------
python import sys
python import vim
python sys.path.append(vim.eval('expand("<sfile>:h")'))

" --------------------------------
"  Function(s)
" --------------------------------

function! ChangeModel()
python << endOfPython

# TODO

endOfPython
endfunction


function! TranslateDebug()
python << endOfPython

from alphacoder import translate_debug

n_best = 5

row, col = vim.current.window.cursor
current_line = vim.current.buffer[row-1]

src = current_line
tgts = translate_debug(src)

vim.current.buffer[row-1:row] = [src] + tgts

new_row = row + len(tgts)
new_col = len(tgts[len(tgts)-1])
vim.current.window.cursor = (new_row, new_col)

endOfPython
endfunction


function! Translate()
python << endOfPython

from alphacoder import translate

n_best = 5

row, col = vim.current.window.cursor
current_line = vim.current.buffer[row-1]

src = current_line
tgts = translate(src)

vim.current.buffer[row-1:row] = [src] + tgts

new_row = row + len(tgts)
new_col = len(tgts[len(tgts)-1])
vim.current.window.cursor = (new_row, new_col)

endOfPython
endfunction



function! TranslateTop()
python << endOfPython

from alphacoder import translate_top

row, col = vim.current.window.cursor
current_line = vim.current.buffer[row-1]

src = current_line
tgt = translate_top(src)

vim.current.buffer[row-1] = tgt
new_col = len(tgt) if '<unk>' not in tgt else tgt.index('<unk>')
vim.current.window.cursor = (row, new_col)

endOfPython
endfunction

" --------------------------------
"  Expose our commands to the user
" --------------------------------
" command! ChangeModel call ChangeModel()
command! TranslateDebug call TranslateDebug()
command! Translate call Translate()
command! TranslateTop call TranslateTop()

" nonrecursive mapping
" nnoremap <c-m> :ChangeModel<cr>
" nnoremap <c-m> <esc>:ChangeModel<cr>

imap <silent> <c-d><c-d><c-d> <esc>:TranslateDebug<cr><esc>A
imap <silent> <c-d><c-d> <esc>:Translate<cr><esc>A
imap <silent> <c-t><c-t> <esc>:TranslateTop<cr><esc>A

nnoremap <c-d><c-d><c-d> :TranslateDebug<cr><esc>A
nnoremap <c-d><c-d> :Translate<cr><esc>A
nnoremap <c-t><c-t> :TranslateTop<cr><esc>A

let g:alphacoder_plugin_loaded = 1
