import json, requests
import re

# settings for model
model_id = 100
n_best = 5

# indentation
rx = re.compile('^\s*')

def request(url, src):
    results = []
    matched = dict()
    try:
        d = json.dumps([{'id': model_id, 'src': src}])
        r = requests.post(url, data=d)
        tgt = r.json()[0]
        for i in range(n_best):
            results.append(tgt[i]['tgt'].encode('utf-8'))
            matched.update(tgt[i]['matched'])
    except Exception as e:
        print e
        # TODO error handling
        # 1. server not running
        # 2. server internal error
        # 3. etc.
    return results, matched

def translate_debug(key):
    # indentation
    m = rx.search(key)
    indent = m.group(0) if m else ''

    # translate (all candidates)
    url = 'http://127.0.0.1:5000/translator/translatedebug'
    results, matched = request(url, key)

    # select all candidates
    top_5 = [indent + '# ' + result for result in results]
    stats = [indent + '#    {}: {}'.format(seq, cnt) for seq, cnt in sorted(matched.iteritems())]

    return top_5 + stats


def translate(key):
    # indentation
    m = rx.search(key)
    indent = m.group(0) if m else ''

    # translate
    url = 'http://127.0.0.1:5000/translator/translate'
    results, matched = request(url, key)

    # select all candidates
    top_5 = [indent + '# ' + result for result in results]

    return top_5

def translate_top(key): # NOT encoded
    # indentation
    m = rx.search(key)
    indent = m.group(0) if m else ''

    # translate
    url = 'http://127.0.0.1:5000/translator/translate'
    results, matched = request(url, key)

    # select top 1 candidate
    top_1 = indent + results[0]

    return top_1 # NOT encoded
