import unittest
import alphacoder as sut


@unittest.skip("Don't forget to test!")
class AlphacoderTests(unittest.TestCase):

    def test_example_fail(self):
        result = sut.alphacoder_example()
        self.assertEqual("Happy Hacking", result)
